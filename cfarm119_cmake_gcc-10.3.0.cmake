# Client maintainer: brad.king@kitware.com
set(CTEST_SITE "cfarm119.cfarm.net")
set(CTEST_BUILD_NAME "AIX-7.3_GCC-10.3.0")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(CTEST_BUILD_FLAGS "-k -j16")
set(CTEST_DASHBOARD_ROOT "${CMAKE_CURRENT_BINARY_DIR}/Builds/GCC_10.3.0")
set(dashboard_model "Nightly")

set(ENV{CC}  /opt/freeware/bin/gcc)
set(ENV{CXX} /opt/freeware/bin/g++)
set(ENV{FC}  /opt/freeware/bin/gfortran)

set(exclude
  # https://gna.org/bugs/?20944
  LinkStatic

  # Insufficient permissions for svnadmin
  CTest.UpdateSVN
  )
string(REPLACE ";" "|" exclude "${exclude}")
set(CTEST_TEST_ARGS
  PARALLEL_LEVEL 4 # 'ulimit -u' is 128
  EXCLUDE "^(${exclude})$"
)

set(dashboard_cache "
CMake_TEST_FILESYSTEM_1S:BOOL=ON
CMAKE_USE_OPENSSL:BOOL=ON
OpenSSL_ROOT:PATH=$ENV{HOME}/common/openssl/3.0.7
")

include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)
