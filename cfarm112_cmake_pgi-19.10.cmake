# Client maintainer: brad.king@kitware.com
set(CTEST_SITE "cfarm112.cfarm.net")
set(CTEST_BUILD_NAME "CentOS7-ppc64le_PGI-19.10")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_BUILD_FLAGS "-k -j20")
set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(CTEST_TEST_ARGS PARALLEL_LEVEL 20)
set(CTEST_DASHBOARD_ROOT "${CMAKE_CURRENT_BINARY_DIR}/Builds/PGI 19.10")
set(dashboard_model Nightly)

set(PGIPREFIX "/opt/cfarm/pgi/linuxpower/19.10")
set(ENV{CC}  "${PGIPREFIX}/bin/pgcc")
set(ENV{FC}  "${PGIPREFIX}/bin/pgfortran")
set(ENV{F90} "${PGIPREFIX}/bin/pgf90")
set(ENV{F77} "${PGIPREFIX}/bin/pgf77")
set(ENV{CPP} "${PGIPREFIX}/bin/pgcc -Mcpp")
set(ENV{CXX} "${PGIPREFIX}/bin/pgc++")

set(dashboard_cache "
CMake_TEST_EXTERNAL_CMAKE:PATH=${CMAKE_CURRENT_BINARY_DIR}/Builds/GCC 8.4/CMake-build/bin

# Needed when dealing with high levels of parallelism
CMake_TEST_CMakeOnly.LinkInterfaceLoop_TIMEOUT:STRING=300

# /usr/lib64/cmake/Qt5* updated on 2018-12-04 now requires cxx_decltype feature
# but CMake does not detect granular features for PGI yet.
CMake_TEST_Qt5:BOOL=OFF
")

include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)
