#
# OS: OSX High Sierra
# CPU: Intel(R) i7 @ 3.6GHz
# GPU: Radeon Pro 560
# Compiler: Apple LLVM version 9.1 (clang-902.0.39.2)
# Maintainer: robert.maynard@kitware.com
#
cmake_minimum_required(VERSION 2.8)

set(maintainer_email_account "robert.maynard")
set(maintainer_email_domain "kitware.org")

set(CTEST_SITE "dragnipur.kitware")
set(CTEST_BUILD_NAME "OSXHighSierra-AppleClang-9.1")

set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "Ninja")

set(build_dir "vtkm_clang")
set(source_dir "${build_dir}_src")
set(CTEST_DASHBOARD_ROOT "$ENV{HOME}/Dashboards/MyTests")
set(CTEST_SOURCE_DIRECTORY "${CTEST_DASHBOARD_ROOT}/${source_dir}")
set(CTEST_BINARY_DIRECTORY "${CTEST_DASHBOARD_ROOT}/${build_dir}")

set(CTEST_UPDATE_COMMAND "git")
set(CTEST_GIT_COMMAND /usr/bin/git)

set(CTEST_USE_LAUNCHERS 1)

ctest_empty_binary_directory(${CTEST_BINARY_DIRECTORY})

# Write initial cache.
file(WRITE "${CTEST_BINARY_DIRECTORY}/CMakeCache.txt" "
CMAKE_BUILD_TYPE:STRING=Release
")

ctest_start(Nightly)
ctest_update(SOURCE "${CTEST_SOURCE_DIRECTORY}")
ctest_configure(BUILD "${CTEST_BINARY_DIRECTORY}" APPEND)
ctest_submit(PARTS Update Configure Notes)
ctest_read_custom_files("${CTEST_BINARY_DIRECTORY}")
ctest_build(BUILD "${CTEST_BINARY_DIRECTORY}" APPEND)
ctest_submit(PARTS Build)
ctest_test(BUILD "${CTEST_BINARY_DIRECTORY}" APPEND PARALLEL_LEVEL 8 SCHEDULE_RANDOM ON)
ctest_submit(PARTS Test)

