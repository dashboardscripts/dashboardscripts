# Client maintainer: brad.king@kitware.com
set(CTEST_SITE "cfarm135.cfarm.net")
set(CTEST_BUILD_NAME "CentOS7-ppc64le_XL-16.1.1")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_BUILD_FLAGS "-k0 -j32")
set(CTEST_CMAKE_GENERATOR "Ninja")
set(CTEST_TEST_ARGS PARALLEL_LEVEL 32)
set(CTEST_DASHBOARD_ROOT "${CMAKE_CURRENT_BINARY_DIR}/Builds/XL 16.1.1")
set(dashboard_model "Nightly")

set(XLCPREFIX "/opt/ibm/xlC/16.1.1")
set(XLFPREFIX "/opt/ibm/xlf/16.1.1")

set(ENV{CC} "${XLCPREFIX}/bin/xlc")
set(ENV{CXX} "${XLCPREFIX}/bin/xlC")
set(ENV{FC} "${XLFPREFIX}/bin/xlf")

set(dashboard_cache "
CMAKE_Fortran_COMPILER:FILEPATH=${XLFPREFIX}/bin/xlf
CMAKE_Fortran_COMPILER_SUPPORTS_F90:BOOL=1

CMake_TEST_EXTERNAL_CMAKE:PATH=${CMAKE_CURRENT_BINARY_DIR}/Builds/GCC 8.3/CMake-build/bin

# Needed when dealing with high levels of parallelism
CMake_TEST_CMakeOnly.LinkInterfaceLoop_TIMEOUT:STRING=300

# /usr/lib64/cmake/Qt5* updated on 1618-12-04 now requires cxx_decltype feature
# but CMake does not detect granular features for XL yet.
CMake_TEST_Qt5:BOOL=OFF
")

include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)
