# Client maintainer: brad.king@kitware.com
set(CTEST_SITE "wopr.kitware.com")
set(CTEST_BUILD_NAME "Solaris-11-x86_Oracle-12.3")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(CTEST_BUILD_FLAGS "-j8")
set(CTEST_TEST_ARGS PARALLEL_LEVEL 8)
set(CTEST_DASHBOARD_ROOT "${CMAKE_CURRENT_BINARY_DIR}/Builds/Oracle 12.3")
set(dashboard_model "Nightly")

set(ENV{AS}  /opt/solarisstudio12.3/bin/sunas)
set(ENV{CC}  /opt/solarisstudio12.3/bin/suncc)
set(ENV{CXX} /opt/solarisstudio12.3/bin/sunCC)
set(ENV{FC}  /opt/solarisstudio12.3/bin/sunf95)

set(dashboard_cache "
CMake_TEST_FindGTK2:BOOL=ON
")
set(dashboard_download_cmake "https://cmake.org/files/dev/cmake-VERSION-sunos-x86_64.tar.gz")

set(dashboard_no_KWSys 1)
include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)
