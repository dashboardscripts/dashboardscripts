#!/bin/bash

# This retrievs the directory of the currently running script in a way that
# should work across GNU and Non-GNU environments alike (Linux, Apple, AIX,
# etc.)
SCRIPT_DIR=$(perl -MCwd -e 'print Cwd::abs_path shift' $(dirname ${BASH_SOURCE}))

# Update ourselves and re-run
cd "${SCRIPT_DIR}"
git log --pretty=format:"%h %aI [%an] %s" | head -1
if [ "${DASHBOARD_SCRIPTS_SKIP_UPDATE}" != "1" ]
then
  cd "${SCRIPT_DIR}"
  if git pull --ff-only
  then
    git submodule update --init --recursive
  fi
  export DASHBOARD_SCRIPTS_SKIP_UPDATE=1
  exec "${BASH_SOURCE}" "$@"
  exit $?
fi

CTEST=${HOME}/common/cmake/latest/bin/ctest

LOG_DIR=${HOME}/Dashboards/CMake/Logs
BASE_DIR=${HOME}/Dashboards/CMake
export TMPDIR=${HOME}/tmp
mkdir -p ${LOG_DIR} ${BASE_DIR} ${TMPDIR}
pushd ${BASE_DIR}

${CTEST} -V -S ${SCRIPT_DIR}/cfarm111_cmake_gcc-8.3.0.cmake 2>&1 | \
  tee ${LOG_DIR}/cfarm111_cmake_gcc-8.3.0.log
${CTEST} -V -S ${SCRIPT_DIR}/cfarm111_cmake_xl-16.1.0.cmake 2>&1 | \
  tee ${LOG_DIR}/cfarm111_cmake_xl-16.1.0.log
${CTEST} -V -S ${SCRIPT_DIR}/cfarm111_cmake_xlclang-16.1.0.cmake 2>&1 | \
  tee ${LOG_DIR}/cfarm111_cmake_xlclang-16.1.0.log
