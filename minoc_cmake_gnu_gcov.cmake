# Client maintainer: brad.king@kitware.com
cmake_minimum_required(VERSION 3.5)

set(CTEST_SITE                  "minoc.kitware")
set(CTEST_BUILD_NAME            "Linux-ninja-gcov")
set(CTEST_BUILD_CONFIGURATION   "Debug")
set(CTEST_BUILD_FLAGS           "")
set(CTEST_CMAKE_GENERATOR       "Ninja")
set(CTEST_TEST_ARGS             PARALLEL_LEVEL 8)
set(CTEST_TEST_CTEST 0)
set(CTEST_COVERAGE_COMMAND      "/usr/bin/gcov")

set(flags "-fprofile-arcs -ftest-coverage -fdiagnostics-show-option -Wall -Wextra -Wshadow -Wpointer-arith -Winvalid-pch -Wcast-align -Wdisabled-optimization -Wwrite-strings -fstack-protector-all -Wconversion -Wno-error=sign-conversion -Wno-error=conversion")

set(ENV{CC} "/usr/bin/gcc")
set(ENV{CXX} "/usr/bin/g++")
set(ENV{FC} "/usr/bin/gfortran")
set(ENV{CFLAGS} "")
set(ENV{CXXFLAGS} "")
set(ENV{FFLAGS} "")
set(ENV{LDFLAGS} "-fprofile-arcs -ftest-coverage")

set(dashboard_model Nightly)
set(dashboard_source_name CMakeGcov)
set(dashboard_binary_name CMakeGcov-build)
set(dashboard_do_coverage ON)

macro(dashboard_hook_start)
  set(dashboard_cache "
CMAKE_C_FLAGS:STRING=${flags}
CMAKE_CXX_FLAGS:STRING=${flags} -Woverloaded-virtual -Wstrict-null-sentinel
CMAKE_SKIP_BOOTSTRAP_TEST:BOOL=TRUE
")
endmacro()

set(dashboard_no_KWSys 1)
include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)
