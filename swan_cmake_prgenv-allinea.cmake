# Client maintainer: chuck.atkins@kitware.com
if("$ENV{CRAYPE_LINK_TYPE}" STREQUAL "")
  set(link_type static)
else()
  set(link_type $ENV{CRAYPE_LINK_TYPE})
endif()

set(CTEST_SITE "swan.cray.com")
if(CMAKE_SYSTEM_PROCESSOR STREQUAL "x86_64")
  set(CTEST_BUILD_NAME "CrayLinux-CrayPE-Allinea-${link_type}")
else()
  set(CTEST_BUILD_NAME "CrayLinux-${CMAKE_SYSTEM_PROCESSOR}-CrayPE-Allinea-${link_type}")
endif()
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_BUILD_FLAGS "-k -j8")
set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(CTEST_DASHBOARD_ROOT "${CMAKE_CURRENT_BINARY_DIR}/Builds/${CMAKE_SYSTEM_PROCESSOR}/PrgEnv Allinea ${link_type}")
set(dashboard_model Nightly)

find_package(EnvModules REQUIRED)
env_module(purge)
env_module(load modules)
env_module(load craype)
env_module(load PrgEnv-allinea)
if(CMAKE_SYSTEM_PROCESSOR STREQUAL "x86_64")
  env_module(load craype-sandybridge)
elseif(CMAKE_SYSTEM_PROCESSOR STREQUAL "aarch64")
  env_module(load craype-arm-thunderx2)
endif()

set(ENV{CC}  cc)
set(ENV{CXX} CC)
set(ENV{FC}  ftn)

set(CTEST_TEST_ARGS
  PARALLEL_LEVEL 8
  EXCLUDE
    "^(Qt[4-5]Deploy|BundleUtilities)$"
    # QtDeploy: Compiler wrappers do weird things with external deps
    # BundleUtilities: System lib deps have 2nd level not searched by DT_RUNPATH
)

set(dashboard_cache "
CMake_TEST_EXTERNAL_CMAKE:PATH=${CMAKE_CURRENT_BINARY_DIR}/Builds/${CMAKE_SYSTEM_PROCESSOR}/GCC 7.5/CMake-build/bin

CMake_TEST_FindEnvModules:BOOL=ON
CMake_TEST_FindGTK2:BOOL=ON

CMake_TEST_Qt4:BOOL=OFF
")

include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)
