# DashboardScripts

A collection of nightly CMake / CTest / CDash dashboard scripts

The scripts in here are organized and named in a particular way such that:

* project-name/ - A git submodule containing the general scripts for a project
* machine-name.(sh|bat) - The nightly script used as a cronjob on a particular machine.
* machine-name_project-name_build-name.cmake - A specific build for a given project.
