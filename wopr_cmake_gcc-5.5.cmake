# Client maintainer: brad.king@kitware.com
set(CTEST_SITE "wopr.kitware.com")
set(CTEST_BUILD_NAME "Solaris-11-x86_GCC-5.5")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(CTEST_BUILD_FLAGS "-j8")
set(CTEST_TEST_ARGS PARALLEL_LEVEL 8)
set(CTEST_DASHBOARD_ROOT "${CMAKE_CURRENT_BINARY_DIR}/Builds/GCC 5.5")
set(dashboard_model "Nightly")

set(ENV{CC}  /opt/csw/gcc5/bin/gcc)
set(ENV{CXX} /opt/csw/gcc5/bin/g++)
set(ENV{FC}  /opt/csw/gcc5/bin/gfortran)

set(dashboard_cache "
CMAKE_USE_OPENSSL:BOOL=ON
CMake_TEST_FindGTK2:BOOL=ON
")

include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)
