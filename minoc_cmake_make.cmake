set(CTEST_SITE "minoc.kitware")
set(CTEST_BUILD_NAME "linux-make")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(CTEST_BUILD_FLAGS -j8)
set(CTEST_TEST_ARGS
  PARALLEL_LEVEL 8
  )

set(valgrind_skip
  /bin/*
  /sbin/*
  /usr/bin/*
  /usr/lib/qt6/bin/*
  bootstrap
  sample_script
  */Tests/CTestTest2/kwsysBin/*
  */Tests/CTestTestCrash/Crash
  *QtAutogen
  )
string(REPLACE ";" "," valgrind_skip "${valgrind_skip}")

set(CTEST_MEMORYCHECK_COMMAND "/usr/bin/valgrind")
set(CTEST_MEMORYCHECK_SUPPRESSIONS_FILE "${CTEST_SCRIPT_DIRECTORY}/minoc_cmake_make_valgrind.supp")
set(CTEST_MEMORYCHECK_COMMAND_OPTIONS
  "--gen-suppressions=all --child-silent-after-fork=yes --trace-children=yes --trace-children-skip=${valgrind_skip} --track-origins=yes -q --leak-check=yes --show-reachable=yes --num-callers=50 -v")

set(dashboard_model Nightly)
set(dashboard_source_name CMakeMake-src)
set(dashboard_binary_name CMakeMake-bld)

set(dashboard_cache "
BUILD_QtDialog:BOOL=ON
CMAKE_SKIP_BOOTSTRAP_TEST:BOOL=TRUE
CMake_TEST_Qt6:BOOL=ON
MEMORYCHECK_COMMAND:FILEPATH=${CTEST_MEMORYCHECK_COMMAND}
MEMORYCHECK_SUPPRESSIONS_FILE:FILEPATH=${CTEST_MEMORYCHECK_SUPPRESSIONS_FILE}
MEMORYCHECK_COMMAND_OPTIONS:STRING=${CTEST_MEMORYCHECK_COMMAND_OPTIONS}
")

# Replace the common script's submit step with our own to leave out "Done".
set(dashboard_no_submit 1)
macro(dashboard_hook_end)
  unset(dashboard_no_submit)
  # Submit all the parts ourselves except "Done".
  ctest_submit(PARTS Update Configure Build Test Notes)
endmacro(dashboard_hook_end)

include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)

# Pick up the CMake build where the common script left off.
ctest_start(Nightly)

#----------------------------------------------------------------------------
# Run valgrind (last because it is slow).

# Shrink stress tests when running with valgrind.
set(ENV{KWSYS_TEST_PROCESS_1_COUNT} 11)

# Exclude some tests from valgrind.
set(exclude
  "RunCMake.CommandLine"  # timeout reached under valgrind
  "RunCMake.ctest_test"  # timeout cases often fail due to memcheck delays
  "RunCMake.install"  # timeout reached under valgrind
  "CMakeLib.testUVProcessChain" # fails spuriously too often
  )
string(REPLACE ";" "|" exclude "${exclude}")

ctest_memcheck(
  PARALLEL_LEVEL 8
  EXCLUDE "^(${exclude})$"
  )
ctest_submit(PARTS MemCheck)

ctest_submit(PARTS Done)
