# Client maintainer: brad.king@kitware.com
set(CTEST_SITE "cfarm112.cfarm.net")
set(CTEST_BUILD_NAME "CentOS7-ppc64le_GCC-8.4")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_BUILD_FLAGS "-k -j20")
set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(CTEST_DASHBOARD_ROOT "${CMAKE_CURRENT_BINARY_DIR}/Builds/GCC 8.4")
set(dashboard_model "Nightly")

set(ENV{CC}  "/opt/at12.0/bin/gcc")
set(ENV{CXX} "/opt/at12.0/bin/g++")
set(ENV{FC}  "/opt/at12.0/bin/gfortran")

set(exclude
  BundleUtilities # LD_LIBRARY_PATH glibc mismatch
  )
string(REPLACE ";" "|" exclude "${exclude}")
set(CTEST_TEST_ARGS
  PARALLEL_LEVEL 20
  EXCLUDE "^(${exclude})$"
)

set(dashboard_cache "
CMAKE_USE_SYSTEM_LIBRARIES:BOOL=ON
CMAKE_USE_SYSTEM_LIBRARY_CPPDAP:BOOL=OFF
CMAKE_USE_SYSTEM_LIBRARY_JSONCPP:BOOL=OFF
CMAKE_USE_SYSTEM_LIBRARY_LIBARCHIVE:BOOL=OFF
CMAKE_USE_SYSTEM_LIBRARY_LIBRHASH:BOOL=OFF
CMAKE_USE_SYSTEM_LIBRARY_LIBUV:BOOL=OFF
CMAKE_USE_SYSTEM_LIBRARY_NLOHMANNJSON:BOOL=OFF
CMAKE_USE_SYSTEM_LIBRARY_ZSTD:BOOL=OFF

# Needed when dealing with high levels of parallelism
CMake_TEST_CMakeOnly.LinkInterfaceLoop_TIMEOUT:STRING=300
")

include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)
