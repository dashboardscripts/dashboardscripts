#!/usr/bin/env bash

export "PATH=${HOME}/Dashboards/bin:$PATH"

"/usr/bin/ctest" -S "${HOME}/Dashboards/DashboardScripts/minoc_cmake_bullseye_cov.cmake" -V >"$HOME/Dashboards/Logs/cmake_bullseye_cov.log" 2>&1

"/usr/bin/ctest" -S "${HOME}/Dashboards/DashboardScripts/minoc_cmake_gnu_gcov.cmake" -V >"$HOME/Dashboards/Logs/cmake_gnu_gcov.log" 2>&1

# Keep last.  This runs tests with valgrind.
"/usr/bin/ctest" -S "${HOME}/Dashboards/DashboardScripts/minoc_cmake_make.cmake" -V >"$HOME/Dashboards/Logs/cmake_make.log" 2>&1
