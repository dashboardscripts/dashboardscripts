# Client maintainer: brad.king@kitware.com
set(CTEST_SITE "cfarm111.cfarm.net")
set(CTEST_BUILD_NAME "AIX-7.1_XL-16.1.0")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(CTEST_BUILD_FLAGS "-k -j16")
set(CTEST_DASHBOARD_ROOT "${CMAKE_CURRENT_BINARY_DIR}/Builds/XL_16.1.0")
set(dashboard_model "Nightly")

set(ENV{CC}  /opt/IBM/xlC/16.1.0/bin/xlc)
set(ENV{CXX} /opt/IBM/xlC/16.1.0/bin/xlC)
set(ENV{FC}  /opt/IBM/xlf/16.1.0/bin/xlf)

set(exclude
  CTestTestTimeout # fails too often
  CTestTestRerunFailed # depends on CTestTestTimeout

  CTest.UpdateSVN # 'svnadmin create' fails

  # RPM tests that fail without objdump installed
  RunCMake.CPack_RPM.DEBUGINFO
  RunCMake.CPack_RPM.EXTRA_SLASH_IN_PATH
  RunCMake.CPack_RPM.SINGLE_DEBUGINFO

  RunCMake.CPack_RPM.SOURCE_PACKAGE # started failing due to change on machine

  RunCMake.ctest_test # fails too often
  )
string(REPLACE ";" "|" exclude "${exclude}")
set(CTEST_TEST_ARGS
  PARALLEL_LEVEL 16
  EXCLUDE "^(${exclude})$"
)

set(dashboard_cache "
CMake_TEST_EXTERNAL_CMAKE:PATH=${CMAKE_CURRENT_BINARY_DIR}/Builds/GCC_8.3.0/CMake-build/bin
CMake_TEST_CMakeOnly.AllFindModules_NO_VERSION:STRING=GETTEXT
CMake_TEST_FILESYSTEM_1S:BOOL=ON
CMAKE_EXECUTABLE_FORMAT:STRING=XCOFF
")

set(dashboard_no_KWSys 1)
include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)
