#!/bin/bash
export DISPLAY=:0.0 # just DISPLAY=:0.0 without export is not enough

/usr/local/bin/ctest -S ${HOME}/Dashboards/DashboardScripts/dragnipur_vtkm_clang.cmake
/usr/local/bin/ctest -S ${HOME}/Dashboards/DashboardScripts/dragnipur_vtkm_icc.cmake
