# Client maintainer: brad.king@kitware.com
set(CTEST_SITE "cfarm119.cfarm.net")
set(CTEST_BUILD_NAME "AIX-7.3_IBMClang-17.1.1")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(CTEST_BUILD_FLAGS "-k -j16")
set(CTEST_DASHBOARD_ROOT "${CMAKE_CURRENT_BINARY_DIR}/Builds/IBMClang_17.1.1")
set(dashboard_model "Nightly")

set(ENV{CC}  /opt/IBM/openxlC/17.1.1/bin/ibm-clang_r)
set(ENV{CXX} /opt/IBM/openxlC/17.1.1/bin/ibm-clang++_r)
unset(ENV{FC})

set(exclude
  # Insufficient permissions for svnadmin
  CTest.UpdateSVN
  )
string(REPLACE ";" "|" exclude "${exclude}")
set(CTEST_TEST_ARGS
  PARALLEL_LEVEL 4 # 'ulimit -u' is 128
  EXCLUDE "^(${exclude})$"
)

set(dashboard_cache "
CMake_TEST_FILESYSTEM_1S:BOOL=ON
CMAKE_Fortran_COMPILER:FILEPATH=
CMAKE_USE_OPENSSL:BOOL=ON
OpenSSL_ROOT:PATH=$ENV{HOME}/common/openssl/3.0.7
")

include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)
