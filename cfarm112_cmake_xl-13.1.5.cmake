# Client maintainer: brad.king@kitware.com
set(CTEST_SITE "cfarm112.cfarm.net")
set(CTEST_BUILD_NAME "CentOS7-ppc64le_XL-13.1.5")
set(CTEST_BUILD_CONFIGURATION Release)
set(CTEST_BUILD_FLAGS "-k -j20")
set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
set(CTEST_TEST_ARGS PARALLEL_LEVEL 20)
set(CTEST_DASHBOARD_ROOT "${CMAKE_CURRENT_BINARY_DIR}/Builds/XL 13.1.5")
set(dashboard_model "Nightly")

set(XLCPREFIX "/opt/ibm/xlC/13.1.5")
set(XLFPREFIX "/opt/ibm/xlf/15.1.5")

set(ENV{CC} "${XLCPREFIX}/bin/xlc")
set(ENV{CXX} "${XLCPREFIX}/bin/xlC")
set(ENV{FC} "${XLFPREFIX}/bin/xlf")
set(dashboard_cache "
CMake_TEST_EXTERNAL_CMAKE:PATH=${CMAKE_CURRENT_BINARY_DIR}/Builds/GCC 8.4/CMake-build/bin

# /usr/lib64/cmake/Qt5* updated on 2018-12-04 now requires cxx_decltype feature
# but CMake does not detect granular features for XL yet.
CMake_TEST_Qt5:BOOL=OFF

# Needed when dealing with high levels of parallelism
CMake_TEST_CMakeOnly.LinkInterfaceLoop_TIMEOUT:STRING=300

# Tests cannot force RPATH over RUNPATH.
CMake_COMPILER_FORCES_NEW_DTAGS:BOOL=ON
")

include(${CTEST_SCRIPT_DIRECTORY}/cmake/cmake_common.cmake)
